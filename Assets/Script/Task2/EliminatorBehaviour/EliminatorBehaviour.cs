﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EliminatorBehaviour : MonoBehaviour
{
    #region Event Calls
    public void Execute(GameObject go)
    {
        var collisionSupport = go.GetComponent<PlayableCollisionSupport>();
        collisionSupport.CollisionEvent?.Invoke();
    }
    #endregion
}
