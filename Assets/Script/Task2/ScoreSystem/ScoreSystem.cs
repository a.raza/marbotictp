﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreSystem : MonoBehaviour
{
    #region Private Variable
    private const int
        DEFAULT_SCORE = 4,
        MIN = 0,
        MAX = 10;
    #endregion

    #region Protected Variable
    protected int
        score,
        index = 0;

    [Space]
    [SerializeField] protected GameManager gameManager;

    [Space]
    [SerializeField] protected UIManager uiManager;

    [Space]
    [SerializeField] protected AudioManager audioManager;
    #endregion

    #region Unity Callbacks
    protected virtual void Awake()
    {
        score = DEFAULT_SCORE;
        DisplayScore();
        gameManager.Add2Reset(ResetScore);
    }
    #endregion

    #region Supporting Functions & Event Calls
    /// <summary>
    /// Will add a point to the current score and will display on the Gameplay screen
    /// </summary>
    protected void GainPoint()
    {
        score++;
        DisplayScore();
        audioManager.Play(3);
    }

    /// <summary>
    /// Will remove a point to the current score and will display on the Gameplay screen
    /// </summary>
    protected void LosePoint()
    {
        score--;
        DisplayScore();
        audioManager.Play(4);
    }

    /// <summary>
    /// Will display score and check on the game status
    /// </summary>
    private void DisplayScore()
    {
        uiManager.DisplayScore(score.ToString());
        GameStatus();
    }

    /// <summary>
    /// This will check the score and enable endScreen with final score and game information to accordingly.
    /// </summary>
    protected virtual void GameStatus()
    {
        if (score <= MIN)
        {
            uiManager.GameWon(false, score);
            audioManager.Play(1);
            GameOver();
        }
        else if (score >= MAX)
        {
            uiManager.GameWon(true, score);
            audioManager.Play(0);
            GameOver();
        }
            //TODO: Fix the bug!
            // But it is not the priority,
            // since it is not mentioned in the list of rules provide by the client.
            //if (IsNeutralOutcome())
            //{
            //    uiManager.NeutralOutcome(score);
            //    audioManager.Play(0);
            //}
    }

    private void GameOver()
    {
        gameManager.ActiveGame = false;
        gameManager.PutGameToSleep();
    }

    /// <summary>
    /// This funcation build to handle the condition tha is,
    /// when all the red balls are put into pockets, and the score is greater than 0
    /// </summary>
    /// <returns></returns>
    private bool IsNeutralOutcome()
    {
        var goods = gameManager.GoodPlayables;

        if(score < MAX && goods.Count > 0)
        {
            foreach (var playable in goods)
            {
                if (playable.IsPlayableActive())
                {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    private void ResetScore()
    {
        StartCoroutine(gameManager.ActivateGame());
        score = DEFAULT_SCORE;
        DisplayScore();
    }
    #endregion
}
