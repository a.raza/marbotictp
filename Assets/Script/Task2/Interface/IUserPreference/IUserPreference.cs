﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public interface IUserPreference
{
    void Perform(UnityAction<Vector3, Vector3> forceLine, UnityAction shotTaken);
}
