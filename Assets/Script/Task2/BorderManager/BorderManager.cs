﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BorderManager : MonoBehaviour
{
    #region Private Variable
    private const int
        MIN = 5,
        MAX = 8;

    [SerializeField] private AudioManager audioManager;
    #endregion

    #region Event Calls
    public void Execute(GameObject go)
    {
        var rand = Random.Range(MIN, MAX);
        audioManager.Play(rand);
    }
    #endregion
}
