﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayableCollisionSupport : MonoBehaviour
{
    private UnityEvent collisionEvent = new UnityEvent();
    public UnityEvent CollisionEvent { get => collisionEvent; set => collisionEvent = value; }
}
