﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RulesOperator : ScoreSystem
{
    #region Unity Callbacks
    protected override void Awake()
    {
        base.Awake();
    }
    #endregion

    #region Event Calls
    public void GoodShot(UnityAction action)
    {
        GainPoint();
        action?.Invoke();
    }

    public void BadShot(UnityAction action)
    {
        LosePoint();
        action?.Invoke();
    }
    #endregion
}
