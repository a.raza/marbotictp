﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    #region Private Variable
    private const int DEFAULT_LIMIT = 10;

    [Range(0f,1f)]
    [SerializeField] private float defaultVolume;

    [SerializeField] private List<Clip> clips = new List<Clip>();
    private List<AudioSource> sources = new List<AudioSource>();
    #endregion

    #region Unity Callbacks
    protected virtual void Awake()
    {
        if (defaultVolume == 0)
            defaultVolume = 0.7f;

        GenerateAudioSources();
    }
    #endregion

    #region Supporting Functions
    public void Play(int id)
    {
        var clip = clips.Find(_track => _track.id == id);

        Execute(clip);
    }

    public void Play(string name)
    {
        var clip = clips.Find(_track => _track.name == name);

        Execute(clip);
    }

    private void Execute(Clip clip)
    {
        if (clip != null)
        {
            var source = GetAvailableSource();

            source.clip = clip.track;
            source.Play();
        }
    }

    private void GenerateAudioSources()
    {
        int index = 0;
        do
        {
            var newSource = gameObject.AddComponent<AudioSource>();
            newSource.volume = defaultVolume;
            sources.Add(newSource);
            index++;

        } while (index < DEFAULT_LIMIT);
    }

    private AudioSource GetAvailableSource()
    {
        foreach(var source in sources)
        {
            if (!source.isPlaying)
                return source;
        }

        var newSource = gameObject.AddComponent<AudioSource>();
        newSource.volume = defaultVolume;
        sources.Add(newSource);
        return newSource;
    }
    #endregion
}
