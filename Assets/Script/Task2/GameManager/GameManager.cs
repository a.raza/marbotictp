﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    #region Private Variable
    private const int POOL = 17;
    private const float
        SPEED = 2.5f,
        MANAGER_ENABLER = 5;

    [SerializeField] private TableOrganizer tableOrganizer;
    private Controller controller;
    private int sleeping = 0;
    private List<Playable>
        playables = new List<Playable>(),
        goodPlayables = new List<Playable>();

    [SerializeField] private bool
        activeGame = false,
        managePlayables = false;

    private UnityEvent onReset = new UnityEvent();
    private Accessibility isControlAccessable = new Accessibility();
    #endregion

    #region Public Properties
    public bool ActiveGame { get => activeGame; set => activeGame = value; }
    public bool ManagePlayables { get => managePlayables; private set => managePlayables = value; }
    public Accessibility IsControlAccessable { get => isControlAccessable; set => isControlAccessable = value; }
    public List<Playable> GoodPlayables { get => goodPlayables; private set => goodPlayables = value; }
    #endregion

    #region Unity Callbacks
    protected virtual void Awake()
    {
        playables = tableOrganizer.CreatePool(() =>
        {
            controller = FindObjectOfType<Controller>();
        },
        out goodPlayables); //Get list of red balls
    }

    protected virtual void Update()
    {
        if (ActiveGame == true)
            if (playables.Count == POOL && ManagePlayables == true && controller != null)
            {
                if (IsGameplayAccessable())
                {
                    Debug.Log("Is Accessable");

                    //Will turn on Green Light, for player to make next move
                    IsControlAccessable?.Invoke(true);

                    controller.enabled = true; //turn on controls
                    ManagePlayables = false;
                }
                else
                {
                    Debug.Log("Not Accessable");
                    //Will turn on Red Light as Game is being managed
                    IsControlAccessable?.Invoke(false);
                }
            }
    }
    #endregion

    #region Supporting Functions
    /// <summary>
    /// Following function will ensure that the balls will not keep on rolling after certin period of time.
    /// </summary>
    /// <returns></returns>
    private bool IsGameplayAccessable()
    {
        sleeping = 0;
        bool value = false;

        foreach (var playable in playables)
        {
            //this will check and manage if any of the balls are not outside of the table
            playable.BorderCheck();

            //check if certain ball needs to be placed back to its previous position on the table
            if (!playable.IsGoodBall() && !playable.IsPlayableActive())
            {
                playable.SetActive(true);
                playable.ResetVelocity();
            }

            //Check if certain ball is still rolling
            if (playable.IsVelocityActive() && playable.IsPlayableActive())
            {
                //this will force the ball to slow down and stop after certain period of time.
                StartCoroutine(ForcedSleep(playable));
                value = false; // don't allow player to access the controls yet..
            }
            else
            {
                sleeping++;
            }
        }

        //check if all the ball are in sleep state..
        if (sleeping == POOL)
            value = true; //allow player to access controls..

        return value;
    }

    /// <summary>
    /// Following method will ensure that all balls starts to slow down after certin period of time.
    /// </summary>
    /// <returns></returns>
    private IEnumerator EnableManager()
    {
        yield return new WaitForSeconds(MANAGER_ENABLER);
        ManagePlayables = true;
    }

    /// <summary>
    /// ForcedSleep method will slowly put ball to sleep, by smoothly bringing velocity values to zero
    /// </summary>
    /// <param name="playable"></param>
    /// <returns></returns>
    private IEnumerator ForcedSleep(Playable playable)
    {
        yield return new WaitForSeconds(Time.smoothDeltaTime);

        if (playable.IsPlayableActive())
        {
            if (playable.Rigidbody.velocity != Vector2.zero)
            {
                playable.Rigidbody.velocity = Vector3.MoveTowards(playable.Rigidbody.velocity, Vector2.zero, SPEED * Time.smoothDeltaTime);
                StartCoroutine(ForcedSleep(playable));
            }
            else
            {
                if (playable.IsPlayableActive())
                    playable.UpdatePreviousAddress();
            }
        }
    }

    /// <summary>
    /// This will put some delay when changing from Menu Screen to Gameplay Screen
    /// </summary>
    /// <returns></returns>
    public IEnumerator ActivateGame(float value = 1f)
    {
        yield return new WaitForSeconds(value);
        ActiveGame = true;
    }

    /// <summary>
    /// This will call EnableManager after player have taken a shot
    /// </summary>
    public void ActiveManager()
    {
        StartCoroutine(EnableManager());
    }
    #endregion

    #region Event Calls
    /// <summary>
    /// This will be used to switch between controls. 0 for Mouse and 1 for Keyboard.
    /// </summary>
    /// <param name="value"></param>
    public void UserPreference(int value)
    {
        controller.UserSelection(value);
        StartCoroutine(ActivateGame());
    }

    public void PutGameToSleep()
    {
        playables.ForEach(p => p.ResetVelocity());
    }

    public void Add2Reset(UnityAction action)
    {
        onReset.AddListener(() => action?.Invoke());
    }

    public void OnReset()
    {
        onReset?.Invoke();
    }
    #endregion
}