﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TableOrganizer : MonoBehaviour
{
    #region Private Variable
    private const int
        COLUMN_LIMIT = 5,
        MIN = 0,
        MAX = 2;

    [SerializeField] private RulesOperator rulesOperator;
    [SerializeField] private GameManager gameManager;

    [Space]
    [Header("Prefabs")]
    [SerializeField] private GameObject badBallPrefab;
    [SerializeField] private GameObject
        goodBallPrefab,
        playablePrefab;

    [Space]
    [SerializeField] private Transform parent;

    [Space]
    [SerializeField] private RectTransform startPoint;
    [SerializeField] private RectTransform
        lastPoint,
        controllablePoint;

    [Space]
    [SerializeField] private float width;
    [SerializeField] private float height;

    private int
        nextBall = 0,
        id = 0;
    #endregion

    #region Supporting Functions 
    /// <summary>
    /// Following will handle the placement of all the balls on the Pool Table.
    /// 
    /// There are 5 column in total, therefore, COLUMN_LIMIT = 5.
    /// 
    /// As loop goes on the amount of balls should also be increase accordingly..
    ///
    ///                                   |   0 0 0 0 0   | - column 4 inner loop will place 5 Balls 
    ///                                   |    0 0 0 0    | - column 3 inner loop will place 4 Balls
    ///                                   |     0 0 0     | - column 2 inner loop will place 3 Balls
    ///                                   |      0 0      | - column 1 inner loop will place 2 Balls
    /// outer loop starts from here --->  |       0       | - column 0 inner loop will place 1 Ball
    /// </summary>
    public List<Playable> CreatePool(UnityAction action, out List<Playable> goodPlayables)
    {
        List<Playable>
            playables = new List<Playable>(),
            _goodPlayables = new List<Playable>();
        //This will be used to randomly select between Yellow and Red ball, which will get placed first. 
        nextBall = Random.Range(MIN, MAX);

        var defaultWidth = width;
        var defaultHeight = height;
        var spaceBetween = width * 2; 
        var startPosition = startPoint.anchoredPosition;
        height = startPosition.y;

        //Outer loop..
        for (int column = 0; column <= COLUMN_LIMIT; column++)
        {
            //Inner loop..
            for (int row = 0; row < column; row++)
            {
                var playable = Create(width, height);
                
                playables.Add(playable);

                if (playable.IsGoodBall())
                    _goodPlayables.Add(playable);

                //Apply space between playables, on X axis
                width -= spaceBetween;
            }

            //Move to next column
            height = startPosition.y + (defaultHeight * column);

            //update the starting position of the X axis.
            width = defaultWidth * column;
        }

        //Placing an extra ball as according to the rules, 8 yellow and 8 red balls.
        playables.Add(Create(lastPoint.anchoredPosition.x, lastPoint.anchoredPosition.y));

        //White Ball
        playables.Add(Create(controllablePoint.anchoredPosition.x, controllablePoint.anchoredPosition.y, playablePrefab));

        goodPlayables = _goodPlayables;

        action?.Invoke();
        return playables;
    }

    /// <summary>
    /// Following funciton will Instantiate each ball and will place it on the Pool Table.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    private Playable Create(float x, float y, GameObject otherPrefab = null)
    {
        var prefab = otherPrefab == null ? GetPrefab() : otherPrefab;
        var go = Instantiate(prefab, parent);
        go.name = string.Format("{0} {1}", prefab.name, id);
        id++;
        var playable = new Playable(new Vector2(x, y), go);
        playable.Reset2StartPoint();
        playable.CollisionSupport.CollisionEvent.RemoveAllListeners();
        playable.CollisionSupport.CollisionEvent.AddListener(() =>
        {
            if (playable.IsGoodBall())
            {
                rulesOperator.GoodShot(() =>
                {
                    playable.Go2PreviousAddress();
                });
            }
            else
            {
                rulesOperator.BadShot(() =>
                {
                    playable.Go2PreviousAddress();
                });
            }
        });

        gameManager.Add2Reset(playable.Reset2StartPoint);
        return playable;
    }

    /// <summary>
    /// This will ensure that 8 yellow and 8 red balls gets placed accordingly on the Pool Table.
    /// </summary>
    /// <returns></returns>
    private GameObject GetPrefab()
    {
        switch (nextBall)
        {
            case 0:
                nextBall = 1;
                return badBallPrefab;
            default:
                nextBall = 0;
                return goodBallPrefab;
        }
    }
    #endregion
}
