﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Clip 
{
    #region Public Variable
    public int id;
    public string name;
    public AudioClip track;
    #endregion
}
