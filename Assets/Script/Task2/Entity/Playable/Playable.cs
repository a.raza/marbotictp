﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class Playable 
{
    #region Private Variable
    private const string GOOD_BALL = "GoodBall";

    private const float
         LEFT_BORDER = 208f,
         RIGHT_BORDER = -208f,
         TOP_BORDER = 510f,
         BOTTOM_BORDER = -510f;


    private RectTransform rect;
    private Rigidbody2D rigidbody;
    private Vector2 previousAddress;
    private PlayableCollisionSupport collisionSupport;
    #endregion

    #region Public Variable
    public Vector2 startPoint;
    public GameObject playable;
    #endregion

    #region Public Properties
    public Rigidbody2D Rigidbody { get => rigidbody; private set => rigidbody = value; }
    public PlayableCollisionSupport CollisionSupport { get => collisionSupport; private set => collisionSupport = value; }
    #endregion

    #region Constructor
    public Playable(Vector2 startPoint, GameObject playable)
    {
        this.startPoint = startPoint;
        this.playable = playable;
        rect = playable.GetComponent<RectTransform>();
        Rigidbody = playable.GetComponent<Rigidbody2D>();
        CollisionSupport = playable.GetComponent<PlayableCollisionSupport>();
    }
    #endregion

    #region Supporting Functions & Event Calls
    /// <summary>
    /// This will reset the position of the ball to where it was at the start of the game
    /// </summary>
    public void Reset2StartPoint()
    {
        SetActive(true);
        ResetVelocity();
        rect.anchoredPosition = startPoint;
        UpdatePreviousAddress();
    }

    /// <summary>
    /// This function will get called when ball hits the pocket.
    /// In addition, this will move the ball to its last location/position.
    /// </summary>
    public void Go2PreviousAddress()
    {
        ResetVelocity();
        rect.anchoredPosition = previousAddress;
        SetActive(false);
    }

    /// <summary>
    /// This will get called when the ball reaches to zero velocity.
    /// In addition, this will update the last location/position of the ball.
    /// </summary>
    public void UpdatePreviousAddress()
    {
        previousAddress = rect.anchoredPosition;
    }

    /// <summary>
    /// This will be used to check if the ball is still rolling
    /// </summary>
    /// <returns></returns>
    public bool IsVelocityActive()
    {
        return !Rigidbody.IsSleeping();
    }

    /// <summary>
    /// This will instantly put the ball to sleep
    /// </summary>
    public void ResetVelocity()
    {
        Rigidbody.Sleep();
    }

    /// <summary>
    /// If the ball can provide +1 points this will return true, else false.
    /// </summary>
    /// <returns></returns>
    public bool IsGoodBall()
    {
        return playable.tag == GOOD_BALL;
    }

    public bool IsPlayableActive()
    {
        return playable.activeInHierarchy;
    }

    public void SetActive(bool value)
    {
        playable.SetActive(value);
    }

    /// <summary>
    /// This will check if the ball is not outside of the table, and will execute accordingly
    /// </summary>
    public void BorderCheck()
    {
        var _trasnform = rect.anchoredPosition;

        if (_trasnform.x >= LEFT_BORDER ||
            _trasnform.x <= RIGHT_BORDER ||
            _trasnform.y >= TOP_BORDER ||
            _trasnform.y <= BOTTOM_BORDER)
        {
            Reset2StartPoint();
        }
    }
    #endregion
}
