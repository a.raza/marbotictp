﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ColliderTriggerDetector : MonoBehaviour
{
    #region Private Variable
    [SerializeField] private Collision action;
    #endregion

    #region Unity Callbacks
    private void OnTriggerEnter2D(Collider2D collision)
    {
        action?.Invoke(collision.gameObject);
    }
    #endregion
}
