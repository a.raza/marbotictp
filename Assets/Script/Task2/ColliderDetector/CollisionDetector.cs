﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetector : MonoBehaviour
{
    #region Private Variable
    [SerializeField] private Collision action;
    #endregion

    #region Unity Callbacks
    private void OnCollisionEnter2D(Collision2D collision)
    {
        action?.Invoke(collision.gameObject);
    }
    #endregion
}
