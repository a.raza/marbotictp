﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    #region Private Variable
    [SerializeField] private GameManager gameManager;
    [SerializeField] private GameObject
        redLight,
        greenLight;

    [Space]
    [SerializeField] private Text scoreDisplay;

    [Space]
    [SerializeField] private GameObject endScreen;
    [SerializeField] private Text
        title,
        info;
    #endregion

    #region Unity Callbacks
    private void Awake()
    {
        OnActive();
        gameManager.IsControlAccessable.AddListener(GameplayAccessibility);
    }
    #endregion

    #region Supporting Functions & Event Calls
    public void DisplayScore(string value)
    {
        scoreDisplay.text = string.Format("Score: {0}", value);
    }

    /// <summary>
    /// This will enable the endScreen and post the information as according to the parameter provided.
    /// </summary>
    /// <param name="value"></param>
    /// <param name="score"></param>
    public void GameWon(bool value, int score)
    {
        if(value == true)
        {
            title.text = "Congratulations!";
            info.text = string.Format("You Won With \n{0} Points!", score);
        }
        else
        {
            title.text = "Sorry!";
            info.text = string.Format("You Made \n{0} Points!", score);
        }
        endScreen.SetActive(true);
    }

    public void NeutralOutcome(int score)
    {
        title.text = "Great Work!";
        info.text = string.Format("You Made With \n{0} Points!", score);
        endScreen.SetActive(true);
    }

    /// <summary>
    /// Will switch between Red and Green Light
    /// </summary>
    /// <param name="value"></param>
    public void GameplayAccessibility(bool value)
    {
        redLight.SetActive(value == false);
        greenLight.SetActive(value == true);
    }

    private void OnActive()
    {
        redLight.SetActive(false);
        greenLight.SetActive(true);
    }
    #endregion
}
