﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class TrajectoryLine : MonoBehaviour
{
    #region Private Variable
    [SerializeField] private LineRenderer
        forceLine,
        targetLine;
    #endregion

    #region Unity Callbacks
    protected virtual void Awake()
    {
        forceLine = GetComponent<LineRenderer>();
    }
    #endregion

    #region Supporting Functions 
    protected void TargetLine(Vector3 startPoint, Vector3 endPoint)
    {
        targetLine.positionCount = 2;
        Vector3[] points = new Vector3[2];
        points[0] = startPoint;
        points[1] = endPoint;

        targetLine.SetPositions(points);
    }

    protected void ForceLine(Vector3 startPoint, Vector3 endPoint)
    {
        forceLine.positionCount = 2;
        Vector3[] points = new Vector3[2];
        points[0] = startPoint;
        points[1] = endPoint;

        forceLine.SetPositions(points);
    }

    protected void EndLine()
    {
        forceLine.positionCount = 0;
        targetLine.positionCount = 0;
    }
    #endregion
}
