﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KeyboardUser : IUserPreference
{
    #region Private Variable
    private Vector2 force;
    private Vector3
        startPoint,
        endPoint,
        currentPoint;

    private float power;
    private Vector2
        minPower,
        maxPower;
    private Rigidbody2D rb;
    private Camera cam;
    private RectTransform rect;
    #endregion

    #region Constructor
    public KeyboardUser(float power, Vector2 minPower, Vector2 maxPower, Rigidbody2D rb, Camera cam, RectTransform rect)
    {
        this.power = power;
        this.minPower = minPower;
        this.maxPower = maxPower;
        this.rb = rb;
        this.cam = cam;
        this.rect = rect;
    }
    #endregion

    #region Supporting Functions
    public void Perform(UnityAction<Vector3, Vector3> forceLine, UnityAction endLine)
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            startPoint = rect.transform.position;
            startPoint.z = 0;
            currentPoint = startPoint;
        }

        if (Input.GetKey(KeyCode.Space))
        {
            if (Input.GetKey(KeyCode.UpArrow))
            {
                currentPoint += Vector3.up * Time.deltaTime;
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                currentPoint += Vector3.down * Time.deltaTime;
            }

            if (Input.GetKey(KeyCode.RightArrow))
            {
                currentPoint += Vector3.right * Time.deltaTime;
            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                currentPoint += Vector3.left * Time.deltaTime;
            }

            forceLine(startPoint, currentPoint);
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            endPoint = currentPoint;

            force = new Vector2
                (
                    Mathf.Clamp(startPoint.x - endPoint.x, minPower.x, maxPower.x),
                    Mathf.Clamp(startPoint.y - endPoint.y, minPower.y, maxPower.y)
                );

            rb.AddForce(force * power, ForceMode2D.Impulse);

            endLine?.Invoke();
        }
    }
    #endregion
}