﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MouseUser : IUserPreference
{
    #region Private Variable
    private Vector2 force;
    private Vector3
        startPoint,
        endPoint,
        currentPoint;

    private float power;
    private Vector2
        minPower,
        maxPower;
    private Rigidbody2D rb;
    private Camera cam;
    private RectTransform rect;
    #endregion

    #region Constructor
    public MouseUser(float power, Vector2 minPower, Vector2 maxPower, Rigidbody2D rb, Camera cam, RectTransform rect)
    {
        this.power = power;
        this.minPower = minPower;
        this.maxPower = maxPower;
        this.rb = rb;
        this.cam = cam;
        this.rect = rect;
    }
    #endregion

    #region Supporting Functions
    public void Perform(UnityAction<Vector3, Vector3> forceLine, UnityAction shotTaken)
    {
        if (Input.GetMouseButtonDown(0))
        {
            startPoint = cam.ScreenToWorldPoint(Input.mousePosition);
            startPoint.z = 0;
        }

        if (Input.GetMouseButton(0))
        {
            currentPoint = cam.ScreenToWorldPoint(Input.mousePosition);
            startPoint.z = 0;
            forceLine(startPoint, currentPoint);
        }

        if (Input.GetMouseButtonUp(0))
        {
            endPoint = cam.ScreenToWorldPoint(Input.mousePosition);
            endPoint.z = 0;

            force = new Vector2
                (
                    Mathf.Clamp(startPoint.x - endPoint.x, minPower.x, maxPower.x),
                    Mathf.Clamp(startPoint.y - endPoint.y, minPower.y, maxPower.y)
                );

            rb.AddForce(force * power, ForceMode2D.Impulse);
            shotTaken?.Invoke();
        }
    }
    #endregion
}
