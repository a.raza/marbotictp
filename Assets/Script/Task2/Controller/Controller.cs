﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : TrajectoryLine
{
    #region Private Variable
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private float power;
    [SerializeField] private Vector2
        minPower,
        maxPower;

    private AudioManager audioManager;
    private GameManager gameManager;
    private Camera cam;
    private RectTransform rect;
    private IUserPreference userPreference;
    #endregion

    #region Unity Callbacks
    protected override void Awake()
    {
        audioManager = FindObjectOfType<AudioManager>();
        rect = GetComponent<RectTransform>();
        gameManager = FindObjectOfType<GameManager>();
        cam = Camera.main;
        EndLine();
        base.Awake();
    }

    private void Update()
    {
        //check if game is active..
        if (gameManager.ActiveGame == true)

            //check if the controls can be accessed and user have chosen the controls ..
            if (gameManager.ManagePlayables == false && userPreference != null)
            {
                userPreference.Perform(ForceLine, ShotTaken);
            }
    }
    #endregion

    #region Supporting Functions
    private void ShotTaken()
    {
        audioManager.Play(2);
        gameManager.IsControlAccessable?.Invoke(false);// turn on the red light
        EndLine();
        gameManager.ActiveManager(); // update GameManager that the shot have been taken
        this.enabled = false; 
    }
    #endregion

    #region Event Calls
    public void UserSelection(int value)
    {
        if(value == 0)
        {
            userPreference = new MouseUser(power, minPower, maxPower, rb, cam, rect);
        }
        else
        {
            userPreference = new KeyboardUser(power, minPower, maxPower, rb, cam, rect);
        }
    }
    #endregion
}