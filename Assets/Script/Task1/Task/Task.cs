﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task : ControlSystem
{
    #region Private Variable
    [SerializeField] private Transform target;
    [SerializeField] private float radians;

    private const float SPEED = 100;
    private bool rotationEnable = false;
    private float degree;
    private Vector3 finalRotationPoint = new Vector3();
    #endregion

    #region Unity Callbacks
    protected override void Awake()
    {
        base.Awake();

        var rad = Mathf.PI / radians;
        degree = rad * Mathf.Rad2Deg;

        Debug.Log(string.Format("radians: {0} ", rad));
        Debug.Log(string.Format("degree: {0} ", degree));
    }

    protected override void Update()
    {
        base.Update();

        Request();
        Behaviour();
    }
    #endregion

    #region Supporting Functions
    private void Request()
    {
        if (requests > 0 && rotationEnable == false)
        {
            var rand = Random.Range(0, 3);

            finalRotationPoint = new Vector3
                (
                    rand == 0 ? degree : 0,
                    rand == 1 ? degree : 0,
                    rand == 2 ? degree : 0
                );

            rotationEnable = true;
        }
    }

    private void Behaviour()
    {
        if (rotationEnable == true)
        {
            target.eulerAngles = Vector3.MoveTowards(target.eulerAngles, finalRotationPoint, SPEED * Time.smoothDeltaTime);

            if (target.eulerAngles == finalRotationPoint)
            {
                rotationEnable = false;
                Debug.Log(string.Format("Task '{0}' ended", requests));
                requests--;
            }
        }
    }
    #endregion
}
