﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ControlSystem : MonoBehaviour
{
    #region Private Variable
    [SerializeField] private KeyCode keyCode;
    #endregion

    #region Protected Variable
    protected int requests = 0;
    #endregion

    #region Unity Callbacks
    protected virtual void Awake() { }

    protected virtual void Update() => RequestControl();
    #endregion

    #region Supporting Functions
    private void RequestControl()
    {
        if (Input.GetKeyDown(keyCode))
        {
            requests++;
            Debug.Log(string.Format("Task '{0}' Created", requests));
        }
    }
    #endregion
}