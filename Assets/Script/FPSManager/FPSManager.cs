﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FPSManager : MonoBehaviour
{
    #region Unity Callbacks
    private void Awake()
    {
        Application.targetFrameRate = 30;
        Screen.SetResolution(1920, 1080, false);
    }
    #endregion
}
